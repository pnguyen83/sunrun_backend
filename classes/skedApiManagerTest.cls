@isTest
private class skedApiManagerTest
{
	
	static testmethod void TestDispatchApi() {
        Map<string, sObject> mapTestData = skedTestDataFactory.createData();
        sked__Resource__c siteSurveyor1 = (sked__Resource__c)mapTestData.get('rsa');
        
        Test.startTest();
        Map<Id, sked__Job__c> mapJob = new Map<Id, sked__Job__c>([SELECT Id FROM sked__Job__c]);
                
        skedMockTest mockClass = new skedMockTest();
		mockClass.resourceId = siteSurveyor1.Id;
		Test.setMock(HttpCalloutMock.class, mockClass);
		
        mockClass.calloutMethod = 'dispatch';
        skedSkeduloApiManager.dispatchJobs(mapJob.keySet());
		
        mockClass.calloutMethod = 'dispatch_error';
        skedSkeduloApiManager.dispatchJobs(mapJob.keySet());
		
        mockClass.calloutMethod = 'dispatch_error_salesforce_side';
        skedSkeduloApiManager.dispatchJobs(mapJob.keySet());
        
        Test.stopTest();
    }

    static testmethod void TestDispatchApi2() {
        Map<string, sObject> mapTestData = skedTestDataFactory.createData();
        sked__Resource__c siteSurveyor1 = (sked__Resource__c)mapTestData.get('rsa');
        
        Test.startTest();
        Map<Id, sked__Job__c> mapJob = new Map<Id, sked__Job__c>([SELECT Id FROM sked__Job__c]);
                
        skedMockTest mockClass = new skedMockTest();
		mockClass.resourceId = siteSurveyor1.Id;
		Test.setMock(HttpCalloutMock.class, mockClass);
		
        mockClass.calloutMethod = 'dispatch';
        skedSkeduloApiManager.dispatchJobs_future(mapJob.keySet());
		
        Test.stopTest();
    }

    static testmethod void TestDispatchApiError1() {
        Map<string, sObject> mapTestData = skedTestDataFactory.createData();
        sked__Resource__c siteSurveyor1 = (sked__Resource__c)mapTestData.get('rsa');
        
        Test.startTest();
        Map<Id, sked__Job__c> mapJob = new Map<Id, sked__Job__c>([SELECT Id FROM sked__Job__c]);
                
        skedMockTest mockClass = new skedMockTest();
		mockClass.resourceId = siteSurveyor1.Id;
		Test.setMock(HttpCalloutMock.class, mockClass);
		
        mockClass.calloutMethod = 'dispatch_error';
        skedSkeduloApiManager.dispatchJobs(mapJob.keySet());
        
        Test.stopTest();
    }

    static testmethod void TestDispatchApiError2() {
        Map<string, sObject> mapTestData = skedTestDataFactory.createData();
        sked__Resource__c siteSurveyor1 = (sked__Resource__c)mapTestData.get('rsa');
        
        Test.startTest();
        Map<Id, sked__Job__c> mapJob = new Map<Id, sked__Job__c>([SELECT Id FROM sked__Job__c]);        
        
        skedMockTest mockClass = new skedMockTest();
		mockClass.resourceId = siteSurveyor1.Id;
		Test.setMock(HttpCalloutMock.class, mockClass);
		
        mockClass.calloutMethod = 'dispatch_error_salesforce_side';
        skedSkeduloApiManager.dispatchJobs(mapJob.keySet());
        
        Test.stopTest();
    }
    
    static testmethod void updateJobTaskTest() {
    	Map<string, sObject> mapTestData = skedTestDataFactory.createData();
    	sked__Job__c job = (sked__Job__c)mapTestData.get('job');
    	test.startTest();
    	skedMockTest mockClass = new skedMockTest();
    	mockClass.calloutMethod = 'jobtask';
    	Test.setMock(HttpCalloutMock.class, mockClass);
    	skedSkeduloApiManager.updateJobTask(job.id);
    	test.stopTest();
    }
}