global class skedModalController extends skedSunRunModel{
	@remoteaction
	public static skedResponse getDashboard(String selectedDate) {
		return skedStoreUtils.getDashBoard(selectedDate, true);
	}

	@remoteaction
	public static skedResponse getListRegions() {
		return skedStoreUtils.getListRegions();
	}

	@remoteaction 
	public static skedResponse getSetting(String storeId, String selectedDate) {
	//public static skedResponse getSetting() {
		return skedStoreUtils.getConfigAPI(storeId, selectedDate);
	}

	@remoteaction
	public static skedResponse getListStores(String selectedDate, String regionId) {
		return skedStoreUtils.getListStores(selectedDate, regionId);
	}

	@remoteaction
	public static skedResponse getListActiveEmployees(String regionId, String selectedDate) {
		return skedStoreUtils.getListActiveEmployees(regionId, selectedDate);
	}

	@remoteaction 
	public static skedResponse getStoreInformation(String regionId, String storeId, String selectedDate) {
		return skedStoreUtils.getStoreInfoManagementConsole(regionId, storeId, selectedDate);
	}

	@remoteaction
	public static skedResponse saveJob(String dateString, Integer startTime, Integer endTime, String storeId, String regionId, String jobId) {
		return skedStoreUtils.createJob(dateString, startTime, endTime, storeId, regionId, jobId);
	}

	@remoteaction
	public static skedResponse assignEmployee(String jobId, String resourceId) {
		return skedStoreUtils.assignEmployee(jobId, resourceId);
	}

	@remoteaction
	public static skedResponse deleteJobAllocation(String allocationId, String jobId) {
		return skedStoreUtils.deleteJobAllocation(allocationId, jobId);
	}

	@remoteaction
	public static skedResponse deleteJob(String jobId) {
		return skedStoreUtils.deleteJob(jobId);
	}

	@remoteaction
	public static skedResponse autoAllocateEmployee(String regionId, String storeId, String selectedDate) {
		return skedStoreUtils.autoAllocateEmployee(regionId, storeId, selectedDate);
	}

	@remoteaction
	public static skedResponse createJobAllocation(List<Allocation> lstAllocations) {
		return skedStoreUtils.createJobAllocation(lstAllocations);
	}
}