@isTest
public class skedTestDataFactory
{
	@isTest
	public static Map<String, sObject> createData()
	{
		Map<String, sObject> mapData = new Map<String, sObject>();
		List<sObject> lstSettings = new List<sObject>();
		List<sObject> lstObject1 = new List<sObject>();
		List<sObject> lstObject2 = new List<sObject>();
		List<sObject> lstObject3 = new List<sObject>();
		List<sObject> lstObject4 = new List<sObject>();
		boolean isSandbox = true;
		String accRecordType = '';

		//===================================================Check environment===========================================//
		isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
		accRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Partner' AND SObjectType = 'Account'].Id;


		//===================================================Create Custom Setting=======================================//
		sked_API_Setting__c apiSetting = new sked_API_Setting__c(
			API_Token__c = '32dd3f3e-6bf1-4def-a7b4-c978c8da790c'
		);
		mapData.put('api setting', apiSetting);
		lstSettings.add(apiSetting);

		sked_Dashboard_Color__c colorSetting = new sked_Dashboard_Color__c(
			Completed_Job__c = '#005fb2',
			Exceed_Time_Allowed__c = '#faa928',
			FSC_Title_Color__c = '#df4040',
			In_Progress_Job__c = '#faa928',
			Manager_Title_Color__c = 'purple',
			No_Assigned_Hour__c = '#349de3',
			Over_Time_Allowed__c = '#df4040',
			Pending_Allocation_Job__c = '#bccdde',
			Ready_Job__c = '#19cf5e',
			RSA_Title_Color__c = '#faa928',
			Within_Time_Allowed__c = '#19cf5e'
		);
		mapData.put('color setting', colorSetting);
		lstSettings.add(colorSetting);

		sked_Dashboard_Settings__c dashboardSetting = new sked_Dashboard_Settings__c(
			Break_Time_Period__c = 30,
			End_Time__c = 2300,
			Job_Time_Period__c = 15,
			Start_Time__c = 0
		);
		mapData.put('dashboar setting', dashboardSetting);
		lstSettings.add(dashboardSetting);

		sked_Shift_Setting__c shiftSetting = new sked_Shift_Setting__c(
			Max_Duration__c = 480,
			Min_Duration__c = 180
		);
		mapData.put('shift setting', shiftSetting);
		lstSettings.add(shiftSetting);

		insert lstSettings;

		//======================================================Create list object 1=======================================//
		//Create region
		sked__Region__c region1 = new sked__Region__c(
			Name = 'South San Diego',
			sked__Timezone__c = 'America/Los_Angeles',
			sked__Country_Code__c = 'US',
			sked_Manager__c = UserInfo.getUserId()
		);
		mapData.put('region 1', region1);
		lstObject1.add(region1);

		//create Holiday
		sked__Holiday__c globalHoliday = new sked__Holiday__c(
			sked__Global__c = true,
			sked__Start_Date__c = System.today().addDays(4),
			sked__End_Date__c = System.today().addDays(4)
		);
		mapData.put('holiday', globalHoliday);
		lstObject1.add(globalHoliday);

		//Create User
		User rsaUser = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Lead Generation - L1'].Id,
		     LastName = 'last',
		     Email = 'puser000@amamama.com',
		     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
		     CompanyName = 'SunRun',
		     Title = 'title',
		     Alias = 'alias',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US'
		    // UserRoleId = r.Id
		);
		mapData.put('rsa user', rsaUser);
		lstObject1.add(rsaUser);

		User fscUser = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales - L1'].Id,
		     LastName = 'last',
		     Email = 'puser000@amamama.com',
		     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
		     CompanyName = 'SunRun',
		     Title = 'title',
		     Alias = 'alias2',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US'
		    // UserRoleId = r.Id
		);
		mapData.put('fsc user', fscUser);
		lstObject1.add(fscUser);

		//Create availability template
		sked__Availability_Template__c avaiTemplate = new sked__Availability_Template__c(
			Name = '7AM-18PM',
			sked__Global__c = true
		);
	    mapData.put('availability template', avaiTemplate);
		lstObject1.add(avaiTemplate); 

		insert lstObject1;

		//=======================================================Create list object 2=====================================//
		//Create account
		Account acc1 = new Account(
			Name = 'Costco - 124 - VISTA - CA',
			Retail_Metro_Market__c = 'San Diego',
			Account_Number__c = '9532202017',
			sked_Region__c = region1.id,
			Classification__c = 'Direct',
			RecordTypeId = accRecordType,
			//Division = 'Sunrun',
			Account_Manager__c = UserInfo.getUserId(),
			Site = 'North San Diego',
			Partnetr_Territory__c = 'SD',
			Customer_ID__c = '124',
			ShippingStreet = '1755 Hacienda Drive',
			ShippingCity = 'Vista',
			ShippingPostalCode = '92081',
			Active__c = true,
			Address_Standardization_Status__c = 'Standardization Required'
		);
		mapData.put('account 1', acc1);
		lstObject2.add(acc1);

		Account acc2 = new Account(
			Name = 'Costco - 124 - VISTA - CA',
			Retail_Metro_Market__c = 'San Diego',
			Account_Number__c = '9532202018',
			//sked_Region__c = region1.id,
			Classification__c = 'Direct',
			RecordTypeId = accRecordType,
			//Division = 'Sunrun',
			//Account_Manager__c = UserInfo.getUserId(),
			Site = 'North San Diego',
			Partnetr_Territory__c = 'SD',
			Customer_ID__c = '124',
			ShippingStreet = '1755 Hacienda Drive',
			ShippingCity = 'Vista',
			ShippingPostalCode = '92081',
			Active__c = true,
			Address_Standardization_Status__c = 'Standardization Required'
		);
		mapData.put('account 2', acc2);
		lstObject2.add(acc2);

		//create availability template entry
		sked__Availability_Template_Entry__c monEntry = new sked__Availability_Template_Entry__c(
			sked__Availability_Template__c = avaiTemplate.Id, 
			sked__Weekday__c = 'MON', 
			sked__Start_Time__c = 700, 
			sked__Finish_Time__c = 1800, 
			sked__Is_Available__c = true
		);
        mapData.put('mon entry', monEntry);
		lstObject2.add(monEntry);
		sked__Availability_Template_Entry__c tueEntry = new sked__Availability_Template_Entry__c(
			sked__Availability_Template__c = avaiTemplate.Id, 
			sked__Weekday__c = 'TUE', 
			sked__Start_Time__c = 700, 
			sked__Finish_Time__c = 1800, 
			sked__Is_Available__c = true
		);
        mapData.put('tue entry', tueEntry);
		lstObject2.add(tueEntry);

        sked__Availability_Template_Entry__c wedEntry = new sked__Availability_Template_Entry__c(
			sked__Availability_Template__c = avaiTemplate.Id, 
			sked__Weekday__c = 'WED', 
			sked__Start_Time__c = 700, 
			sked__Finish_Time__c = 1800, 
			sked__Is_Available__c = true
		);
        mapData.put('wed entry', wedEntry);
		lstObject2.add(wedEntry);

		sked__Availability_Template_Entry__c thuEntry = new sked__Availability_Template_Entry__c(
			sked__Availability_Template__c = avaiTemplate.Id, 
			sked__Weekday__c = 'THU', 
			sked__Start_Time__c = 700, 
			sked__Finish_Time__c = 1800, 
			sked__Is_Available__c = true
		);
        mapData.put('thu entry', thuEntry);
		lstObject2.add(thuEntry);

		sked__Availability_Template_Entry__c friEntry = new sked__Availability_Template_Entry__c(
			sked__Availability_Template__c = avaiTemplate.Id, 
			sked__Weekday__c = 'FRI', 
			sked__Start_Time__c = 700, 
			sked__Finish_Time__c = 1800, 
			sked__Is_Available__c = true
		);
        mapData.put('fri entry', friEntry);
		lstObject2.add(friEntry);

		//create region holiday
		sked__Holiday_Region__c regionHoliday = new sked__Holiday_Region__c(
			sked__Holiday__c = globalHoliday.id,
			sked__Region__c = region1.id
		);
		mapData.put('region holiday', regionHoliday);
		lstObject2.add(regionHoliday);

		insert lstObject2;

		//=======================================================Create list object 3====================================//
		//create retail store slot
		for (Integer i = 0; i < 7; i++) {
			sked_Retail_Store_Slot__c slot = new sked_Retail_Store_Slot__c(
				name = 'Costco - 124 - VISTA - CA',
				sked_Retail_Store__c = acc1.id,
				sked_Territory__c = region1.id,
				sked_Kiosk_Location__c = 'Roadshow',
				sked_Date__c = System.today().addDays(i),
				sked_Start__c = 930,
				sked_End__c = 2130
			);
			mapData.put('retail store slot' + i, slot);
			lstObject3.add(slot);
		}

		//Create resources
		sked__Resource__c rsa = new sked__Resource__c(
			Name = 'test resource 1',
			sked__Resource_Type__c = 'Person',
			sked__User__c = rsaUser.id,
			sked__Primary_Region__c = region1.id,
			sked__Category__c = 'Retail Solar Advisor',
			sked__Email__c = 'test@abc.com',
			sked__Mobile_Phone__c = '0123456789',
			sked__Notification_Type__c = 'sms',
			sked__Country_Code__c = 'US',
			sked__Auto_Schedule__c = true,
			sked__Home_Address__c = '3720 Hemlock Street, San Diego, CA 92113',
			sked__GeoLocation__latitude__s = 32.748346,
			sked__GeoLocation__longitude__s = -117.094408,
			sked__Is_Active__c = true,
			sked_Sun_Run_trained__c = true,
			sked__Weekly_Hours__c = 40
		);
		mapData.put('rsa', rsa);
		lstObject3.add(rsa);

		sked__Resource__c fsc = new sked__Resource__c(
			Name = 'test resource 2',
			sked__Resource_Type__c = 'Person',
			sked__User__c = fscUser.id,
			sked__Primary_Region__c = region1.id,
			sked__Category__c = 'Field Sales Consultant',
			sked__Email__c = 'test2@abc.com',
			sked__Mobile_Phone__c = '00123456789',
			sked__Notification_Type__c = 'sms',
			sked__Country_Code__c = 'US',
			sked__Auto_Schedule__c = true,
			sked__Home_Address__c = '5169 Choc Cliff Drive, Bonita, CA 91902',
			sked__GeoLocation__latitude__s = 32.661216,
			sked__GeoLocation__longitude__s = -117.016114,
			sked__Is_Active__c = true,
			sked_Sun_Run_trained__c = true,
			sked__Weekly_Hours__c = 8
		);
		mapData.put('fsc', fsc);
		lstObject3.add(fsc);

		//Create Job 
		sked__Job__c job = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today().addDays(1), Time.newInstance(11, 0, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today().addDays(1), Time.newInstance(12, 0, 0, 0)),
			sked__Duration__c = 60,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION
		);
		mapData.put('job', job);
		lstObject3.add(job);

		sked__Job__c job1 = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(10, 0, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today(), Time.newInstance(14, 0, 0, 0)),
			sked__Duration__c = 240,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION
		);
		mapData.put('job 1', job1);
		lstObject3.add(job1);

		sked__Job__c joba = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(13, 30, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today(), Time.newInstance(18, 0, 0, 0)),
			sked__Duration__c = 270,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION
		);
		mapData.put('job a', joba);
		lstObject3.add(joba);

		sked__Job__c jobb = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(8, 0, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today(), Time.newInstance(10, 30, 0, 0)),
			sked__Duration__c = 150,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION
		);
		mapData.put('job b', jobb);
		lstObject3.add(jobb);

		sked__Job__c job2 = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today().addDays(1), Time.newInstance(10, 0, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today().addDays(1), Time.newInstance(14, 0, 0, 0)),
			sked__Duration__c = 240,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_READY
		);
		mapData.put('job 2', job2);
		lstObject3.add(job2);

		sked__Job__c job3 = new sked__Job__c(
			sked__Type__c = 'Shift',
			sked__Address__c = '12155 Tech Center Drive, Poway, 92064',
			sked__GeoLocation__latitude__s = 32.9360985,
			sked__GeoLocation__longitude__s = -117.0383096,
			sked__Region__c = region1.id,
			sked__Start__c = DateTime.newInstance(System.today().addDays(2), Time.newInstance(11, 0, 0, 0)),
			sked__Finish__c = DateTime.newInstance(System.today().addDays(2), Time.newInstance(15, 0, 0, 0)),
			sked__Duration__c = 240,
			sked__Account__c = acc1.Id,
			sked__Job_Status__c = skedConstants.JOB_STATUS_READY
		);
		mapData.put('job 3', job3);
		lstObject3.add(job3);

		insert lstObject3;
		
		//=======================================================Create list object 4===================================//
		//create job allocation
		sked__Job_Allocation__c allocation1 = new sked__Job_Allocation__c(
			sked__Job__c = job2.id,
			sked__Resource__c = rsa.id,
			sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
		);	
		mapData.put('allocation 1', allocation1);
		lstObject4.add(allocation1);

		sked__Job_Allocation__c allocation2 = new sked__Job_Allocation__c(
			sked__Job__c = job3.id,
			sked__Resource__c = fsc.id,
			sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
		);	
		mapData.put('allocation 2', allocation2);
		lstObject4.add(allocation2);	

		sked__Job_Allocation__c allocation3 = new sked__Job_Allocation__c(
			sked__Job__c = job3.id,
			sked__Resource__c = rsa.id,
			sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
		);	
		mapData.put('allocation 3', allocation3);
		lstObject4.add(allocation3);	

		//Create activity
		sked__Activity__c acti1 = new sked__Activity__c(
			sked__Address__c = '2815 Diamond St, San Francisco, CA 94131',
			sked__Type__c = 'Travel',
			sked__Start__c = DateTime.newInstance(System.today(), Time.newInstance(8, 30, 0, 0)),
			sked__End__c = DateTime.newInstance(System.today(), Time.newInstance(17, 30, 0, 0)),
			sked__Timezone__c = 'America/Los_Angeles',
			sked__Resource__c = rsa.id
		);	
		mapData.put('activity 1', acti1);
		lstObject4.add(acti1);

		sked__Activity__c acti2 = new sked__Activity__c(
			sked__Address__c = '2815 Diamond St, San Francisco, CA 94131',
			sked__Type__c = 'Travel',
			sked__Start__c = DateTime.newInstance(System.today().addDays(4), Time.newInstance(8, 30, 0, 0)),
			sked__End__c = DateTime.newInstance(System.today().addDays(4), Time.newInstance(17, 30, 0, 0)),
			sked__Timezone__c = 'America/Los_Angeles',
			sked__Resource__c = fsc.id
		);	
		mapData.put('activity 2', acti2);
		lstObject4.add(acti2);

		//create resource availability template
		sked__Availability_Template_Resource__c rsaTemplate = new sked__Availability_Template_Resource__c(
			sked__Availability_Template__c = avaiTemplate.id,
			sked__Resource__c = rsa.id
		);
		mapData.put('rsa availability', rsaTemplate);
		lstObject4.add(rsaTemplate);

		sked__Availability_Template_Resource__c fscTemplate = new sked__Availability_Template_Resource__c(
			sked__Availability_Template__c = avaiTemplate.id,
			sked__Resource__c = fsc.id
		);
		mapData.put('fsc availability', fscTemplate);
		lstObject4.add(fscTemplate);

		//create availability
		sked__Availability__c rsaAvai = new sked__Availability__c(
			sked__Finish__c = DateTime.newInstance(Date.today().addDays(2), Time.newInstance(16, 0, 0, 0)),
			sked__Is_Available__c = true,
			sked__Resource__c = rsa.id,
			sked__Start__c = DateTime.newInstance(Date.today().addDays(2), Time.newInstance(9, 0, 0, 0)),
			sked__Status__c = 'Approved',
			sked__Type__c = 'Leave'
		);
		mapData.put('rsa leave', rsaAvai);
		lstObject4.add(rsaAvai);

		sked__Availability__c rsaAvai2 = new sked__Availability__c(
			sked__Finish__c = DateTime.newInstance(Date.today().addDays(3), Time.newInstance(12, 0, 0, 0)),
			sked__Is_Available__c = false,
			sked__Resource__c = rsa.id,
			sked__Start__c = DateTime.newInstance(Date.today().addDays(3), Time.newInstance(9, 0, 0, 0)),
			sked__Status__c = 'Approved',
			sked__Type__c = 'Leave'
		);
		mapData.put('rsa leave 2', rsaAvai2);
		lstObject4.add(rsaAvai2);
		insert lstObject4;

		return mapData;
	}
}