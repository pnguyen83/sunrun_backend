public class skedConstants {
    public static final String USER_WITHOUT_RESOURCE = 'The current user is not associated with any region';
    public static final String NO_DASHBOAD_SETTING = 'Cannot get value from Sked Dashboard Settings custom setting';
    public static final String NO_SHIFT_TIME_SETTING = 'Cannot get value from Sked Shift Settings custom setting';
    public static final String NO_COLOR_SETTING = 'Cannot get value from Sked Dashboard Color custom setting';
    public static final String NO_REGION = 'Cannot get region infomation from region id: ';
    public static final String JOB_ID_INVALID = 'Cannot get job information from job id: ';
    public static final String FSC_OVER_ALLOWED_HOURS = 'Total allocated hours are over the allowed hours in a week for this employee: ';
    public static final String FIRST_JOB_OVER_SLOT_TIME = 'The first shift cannot start before the scheduled day start time';
    public static final String LAST_JOB_OVER_SLOT_TIME = 'The last shift cannot end after the scheduled day end time';
    public static final String JOB_OVERLAP_30_MINUTES = 'No shifts can overlap over a period of 30 minutes';
    public static final String MORE_THAN_2_JOBS_OVERLAP = 'No more than two shifts can overlap each other';
    public static final String NO_EMPLOYEES = 'There are no active employees for this region';
    public static final String CREATE_JOB_IN_THE_PAST = 'Cannot create a shift in the past';
    public static final String INVALID_JOB_ALLOCATION_ID = 'Invalid job allocation id';
    public static final String INVALID_JOB_ID = 'Invalid job id';
    public static final String INVALID_STORE_ID = 'Invalid store id';
    public static final String NO_AVAI_RESOURCE = 'No available employee for this shift';
    public static final String JOB_DURATION_UNDER_3_HOURS = 'A single shift can’t be less than 3 hours in duration';
    public static final String JOB_DURATION_OVER_8_HOURS = 'A single shift can’t exceed 9 hours in duration';
    public static final String JOB_DUPLICATE = 'This shift is duplicated with another shift';
    public static final String ASSIGN_PAST_JOB = 'Cannot assign employee to a shift in the past: ';
    public static final String FSC_OVER_4_HOURS_A_SHIFT = 'An FSC is not allowed to be allocated to a shift greater than 6 hours.';
    public static final String RSA_OVER_8_HOURS_A_DAY = '3 The total assigned hours including the new shift are over the allowed hours in a day for this employee.';
    public static final String JOB_IN_PROGRESS = 'This shift is in progress. Please refresh the page.';
        
    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final Set<String> JOB_STATUS_ON_GOING = new Set<String>{'En Route','On Site','In Progress'};
    public static final Set<String> JOB_NOT_COMMENCED = new Set<String>{'Pending Dispatched','Dispatched','Confirmed'};
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_OPEN = 'Open';
    public static final string JOB_ALLOCATION_STATUS_CHECK_IN = 'Checked In';

    public static final string MOBILE_CHECKED_IN = 'CHECKEDIN';
    public static final string MOBILE_EN_ROUTE = 'ENROUTE';
    public static final string MOBILE_COMPLETE = 'COMPLETE';
    public static final string MOBILE_IN_PROGRESS = 'INPROGRESS';

    public static final string SCHEDULED_DAY_CANCELLED_SUNRUN = 'Cancelled by Sunrun';
    public static final string SCHEDULED_DAY_CANCELLED_STORE = 'Cancelled by Retailer';

    public static final string FSC_RESOURCE = 'Field Sales Consultant';
    public static final string RSA_RESOURCE = 'Retail Solar Advisor';
    public static final string REP_RESOURCE = 'REP';

    public static final string REP_TITLE = 'REP';
    public static final string RSA_TITLE = 'RSA';
    public static final string FSC_TITLE = 'FSC';

    public static final String ACCOUNT_RECORD_TYPE = 'Partner';

    public static final String JOB_TYPE_SHIFT = 'Shift';

    public static final String NO_JOB_TYPE_SHIFT = 'Error: There is not any Job Template for Job Type Shift';

    public static final String JOB_TASK_TYPE = 'JobTasks';

    public static final String JOB_CAN_BE_DECLINED = 'CanBeDeclined';

    public static integer RSA_WEEK_HOUR = 40;
    public static integer RSA_MAX_SHIFT_LENGTH = 9;

    public static integer FSC_WEEK_HOUR = 12;
    public static integer FSC_DAY_HOUR = 6;
    public static integer FSC_MAX_SHIFT_LENGTH = 360;

    //public static integer JOB_MIN_DURATION = 180;
    //public static integer JOB_MAX_DURATION = 540;
    public static integer RSA_JOB_MAX_DURATION = 540;

    public static integer JOB_OVERLAP_TIME = 30;

    public static integer NO_JOB_OVERLAP = 2;
}