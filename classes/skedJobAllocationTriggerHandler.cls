public class skedJobAllocationTriggerHandler extends skedSunRunModel{
	public static void onBeforeUpdate(Map<Id, sked__Job_Allocation__c> mapNew, Map<Id, sked__Job_Allocation__c> mapOld) {
		updateAllocationHours(mapNew.values());
	}

	public static void onAfterUpdate(Map<Id, sked__Job_Allocation__c> mapNew, Map<Id, sked__Job_Allocation__c> mapOld) {
		updateJobDistance(mapNew, mapOld);
	}

	//=====================================================Execution======================================================//
	public static void updateAllocationHours(List<sked__Job_Allocation__c> lstNew) {
		for (sked__Job_Allocation__c ja: lstNew) {
            // Only calculate when Job Allocation status is Complete
            if (ja.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_COMPLETE) {
            	// Only calculate when In Progress and Completed both have a value
                if (ja.sked__Time_In_Progress__c != null && ja.sked__Time_Completed__c != null) {
                	// Calculate the hours difference between Completed and In Progress datetimes
                    Double dElapsedMinutes = (ja.sked__Time_Completed__c.getTime() - ja.sked__Time_In_Progress__c.getTime()) / 1000 / 60;
                    
                    // Needed to calculate hours separately as including in above expression did not calculate correctly 
                    ja.sked__Hours__c = dElapsedMinutes / 60;
                    
                    // Round to 1 decimal place
                    ja.sked__Hours__c = ja.sked__Hours__c.setScale(1);
                } else {
                    // Reset to nothing if either or both of the In Progess or Completed datetimes are empty
                    ja.sked__Hours__c = null;
                }
            }
        }
	}



	public static void updateJobDistance(Map<Id, sked__Job_Allocation__c> mapNew, Map<Id, sked__Job_Allocation__c> mapOld) {
		Set<Id> setAllocationIds = new Set<Id>();

		for (sked__Job_Allocation__c ja : mapNew.values()) {
			if (mapOld.containsKey(ja.Id)) {
				sked__Job_Allocation__c oldJA = mapOld.get(ja.Id);
				
				if (ja.sked__Status__c != null && ja.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_CHECK_IN && 
					(oldJA.sked__Status__c == null || oldJA.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_CHECK_IN)) {
					setAllocationIds.add(ja.id);
				}

				if (ja.sked__Status__c != null && ja.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_COMPLETE && 
					(oldJA.sked__Status__c == null || oldJA.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_COMPLETE)) {
					setAllocationIds.add(ja.id);
				}
			}
		}

		if (!setAllocationIds.isEmpty()) {
			skedSkeduloApiManager.updateJobDistance(setAllocationIds);
		}
	}
}