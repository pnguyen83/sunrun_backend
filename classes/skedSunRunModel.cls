global virtual class skedSunRunModel {
	public class DashboardModel {
		public List<StoreModel> lstStores;
		public List<EmployeeModel> lstEmployees;

		public DashboardModel() {
			this.lstStores = new List<StoreModel>();
			this.lstEmployees = new List<EmployeeModel>();
		}
	}

	public class SettingModel {
		public List<PickListItem> lstRegions;
		public List<TimeModel> start;
		public List<TimeModel> finish;
		public Decimal breakTime;
		public Decimal timeStep;
		public List<PickListItem> lstStores;
		public StoreModel selectedStore;
		
		public SettingModel() {
			this.lstRegions = new List<PickListItem>();
			//this.start = new List<TimeModel>();
			//this.finish = new List<TimeModel>();
			this.lstStores = new List<PickListItem>();
		}
	}

	public class StoreModel {
		public String id;
		public String name;
		public String regionId;
		public String managerId;
		public DateTime startTime;
		public DateTime endTime;
		public TimeModel start;
		public TimeModel finish;

		public List<DateSlot> lstDateSlots;
		public List<JobModel> lstJobs;

		public StoreModel(Account acc, ColorModel color, DateTime startDate, DateTime endDate, String timezone) {
			this.id = acc.id;
			this.name = acc.Name;
			//this.managerId = acc.Account_Manager__c;
			this.lstDateSlots = initDateSlots(this.lstDateSlots, startDate, endDate, timezone, color);
			this.lstJobs = new List<JobModel>();
			this.regionId = acc.sked_Region__c;
		}

		public StoreModel() {

		}
	}

	global class EmployeeModel implements Comparable{
		public String id;
		public String name;
		public String category;
		public String employeeType;
		public String regionId;
		public String userId;
		public Boolean isREP;
		public boolean isFSC;
		public boolean isRSA;
		public String storeId;
		public String avatar;
		public Item title;
		public Item totalAssignedHours;
		public Boolean isNotAllow;
		public TimeModel startTime;
		public TimeModel endTime;
		public String timezone;
		public Boolean isAllocated;
		
		public List<DateSlot> lstDateSlots;
		public transient Location geoLocation {get;set;}
        public Map<String, List<Availability>> mapTemplateEntries;
        public Map<String, List<Availability>> mapAvailabilityRecords;
        public Map<String, List<Availability>> mapActivityRecords;
        public Map<String, List<Availability>> mapUnAvailabilityRecords;
        public Map<String, List<Availability>> mapJobAllocations;

        public EmployeeModel(sked__Resource__c skedResource, DateTime startDate, DateTime endDate, String timezone, ColorModel color) {
        	this.isREP = false;
        	this.isFSC = false;
        	this.isRSA = false;
        	this.isNotAllow = false;
            this.id = skedResource.Id;
            this.name = skedResource.Name;
            this.mapTemplateEntries = new Map<String, List<Availability>>();
            this.mapAvailabilityRecords = new Map<String, List<Availability>>();
            this.mapActivityRecords = new Map<String, List<Availability>>();
            this.mapUnAvailabilityRecords = new Map<String, List<Availability>>();
            this.mapJobAllocations = new Map<String, List<Availability>>();
           	this.lstDateSlots = initDateSlots(this.lstDateSlots, startDate, endDate, timezone, color);
            this.regionId = skedResource.sked__Primary_Region__c;
            this.userId = skedResource.sked__User__c;
            this.avatar = skedResource.sked__User__r.SmallPhotoUrl;
            this.category = skedResource.sked__Category__c;
            this.employeeType = skedResource.sked__Category__c;
            this.geoLocation = skedResource.sked__GeoLocation__c;
            this.totalAssignedHours = new Item(0, color.noAssignedHours);
            this.timezone = timezone;
            this.isAllocated = false;

            if (skedResource.sked__Category__c == null) {
                throw new skedException('This resource does not have Category: ' + skedResource.Name);
            }
            //set the type of resource and color of resource
            if (skedResource.sked__Category__c.equals(skedConstants.REP_RESOURCE)) {
            	this.isREP = true;
            	this.title = new Item(skedConstants.REP_TITLE, color.repTitle);
            } else if (skedResource.sked__Category__c.equals(skedConstants.FSC_RESOURCE)) {
            	this.isFSC = true;
            	this.title = new Item(skedConstants.FSC_TITLE, color.fscTitle);
            } else if (skedResource.sked__Category__c.equals(skedConstants.RSA_RESOURCE)) {
            	this.isRSA = true;
            	this.title = new Item(skedConstants.RSA_TITLE, color.rsaTitle);
            }
        }

        global Integer compareTo(Object compareTo) {
        	EmployeeModel emp2 = (EmployeeModel)compareTo;
        	Decimal total1 = this.totalAssignedHours.value;
        	Decimal total2 = emp2.totalAssignedHours.value;
        	if (total1 > total2) {
        		return 1;
        	} 
        	if (total1 == total2) {
        		return 0;
        	}

        	return -1;
        }
        
	}

	public class JobModel {
		public String id;
		public String storeId;
		public DateTime startTime;
		public DateTime endTime;
		public String jobDate;
		public TimeModel start;
		public TimeModel finish;
		public String resourceId;
		public EmployeeModel assignedTo;
		public String allocationId;
		public Decimal duration;
		public Item title;
		public Boolean isAllocated;
		public String jobStatus;
		public String storeName;

		public JobModel(sked__job__c data, String timezone) {
			this.isAllocated = false;
			this.id = data.id;
			System.debug('1aa jobid = ' + data.id);
			System.debug('1aa start = ' + data.sked__start__c);
			System.debug('1aa finish = ' + data.sked__finish__c);
			System.debug('1aa 2 timezone = ' + timezone);
			this.storeid = data.sked__Account__c;
			this.startTime = data.sked__start__c;
			this.endTime = data.sked__finish__c;
			this.jobDate = String.valueOf(data.sked__start__c.Date());
			this.start = new TimeModel(data.sked__start__c, timezone);
			this.finish = new TimeModel(data.sked__finish__c, timezone);
			this.duration = data.sked__Duration__c/60;
			this.jobStatus = data.sked__Job_Status__c;
			String labelStart = this.start.label;
			String labelFinish = this.finish.label;
			String jobTitle = labelStart + ' - ' + labelFinish;
			String jobColor = setJobTitleColor(this.jobStatus);
			this.title = new Item(jobTitle, jobColor);
			this.storeName = data.sked__Account__r.Name != null ? data.sked__Account__r.Name : '';
		}

		private String setJobTitleColor(String jobStatus) {
			String jobColor = '';
			ColorModel color = new ColorModel();

			if (jobStatus.equalsIgnoreCase(skedConstants.JOB_STATUS_PENDING_ALLOCATION)) {
				jobColor = color.pendingJob;
			} else if (jobStatus.equalsIgnoreCase(skedConstants.JOB_STATUS_READY) || 
				jobStatus.equalsIgnoreCase(skedConstants.JOB_STATUS_DISPATCHED)) {
				jobColor = color.readyJob;
			} else if (jobStatus.equalsIgnoreCase(skedConstants.JOB_STATUS_COMPLETE)) {
				jobColor = color.completedJob;
			} else if (skedConstants.JOB_STATUS_ON_GOING.contains(jobStatus)) {
				jobColor = color.inProgressJob;
			}
			return jobColor;
		}
	}

	public class AutoAllocateModel {
		public List<Allocation> lstCanAllocateJobs;
		public List<Allocation> lstCannotAllocateJobs;

		public AutoAllocateModel () {
			this.lstCanAllocateJobs = new List<Allocation>();
			this.lstCannotAllocateJobs = new List<Allocation>();
		}
	}

	public class DeleteAllocation {
		public JobModel job;
		public List<EmployeeModel> lstEmployees;

		public DeleteAllocation() {
			this.lstEmployees = new List<EmployeeModel>();
		}
	}

	public class Allocation {
		public String id;
		public EmployeeModel employee;
		public String jobId;
		public String jobTitle;
		public String resourceId;
		public String resourceName;
		public boolean isAllowAllocate;
		public Decimal noEmployees;
		public String message;
		public JobModel job;
		public String storeName;

		public Allocation() {

		}

		public Allocation(sked__Job_Allocation__c jobAll) {
			this.noEmployees = 0;
			this.resourceId = jobAll.sked__Resource__c;
			this.isAllowAllocate = false;
		}

		public Allocation (JobModel job, String timezone) {
			String weekDay = job.startTime.format('EEEEE', timezone);
			String localDate = job.startTime.Date().format();
			this.jobId = job.id;
			this.jobTitle = weekDay + ' ' + localDate + ', ' + job.start.label + ' - ' + job.finish.label + ' (' + job.duration + ' hour(s))';
			this.isAllowAllocate = false;
			this.storeName = String.isNotBlank(job.storeName) ? job.storeName : ''; 
		}
	}

	public class PickListItem {
        public string id    {get;set;}
        public string label {get;set;}
        public boolean isSelected;

        public PickListItem(string id, string label){
            this.id     = id;
            this.label  = label;
            this.isSelected = false;
        }

        public PickListItem(sObject obj){
            this.id     = (string)obj.get('id');
            this.label  = (string)obj.get('name');
            this.isSelected = false;
        }
    }

	public class Item {
		public String color;
		public Decimal value;
		public String title;

		public Item (String title, String color) {
			this.title = title;
			this.color = color;
		}

		public Item(Decimal hours, String color) {
			this.value = hours;
			this.color = color;
		}
	}

	public class DateSlot {
		public String title;
		public String weekDate;
		public Item nAvaiResource;
		public String slotType;
		//public Item color;
		public Item assignedHours;
		public DateTime slotStart;
		public DateTime slotFinish;
		public TimeModel startTime;
		public TimeModel endTime;
		public String dateString;
		public Date dateValue;
		public Boolean isAvailable;
		public String retailStoreSlotId;
		public String location;

		public Map<String, EmployeeModel> mapAvaiResources;
		public List<EmployeeModel> lstAvaiResources;
		public List<JobModel> lstJobs;
	}

	public class TimeModel {
		public Integer id;
		public String label;

		public TimeModel(){}

		public TimeModel (DateTime dt, String timezone) {
            System.debug('timezone = ' + timezone);
            System.debug('dt = ' + dt);
			String d = dt==null?'':dt.format('Hmm', timezone);//Hour in day (0-23)
            
            if(d==''){
                this.id = 0;
            }else{
                this.id = Integer.valueOf(d);
            }
            
            this.label = dt==null?'':dt.format('h:mma', timezone);//Hour in am/pm (1-12)
            
        }

        public TimeModel cloneMe(){
        	TimeModel newTime = new TimeModel();
        	newTime.id = this.id;
        	newTime.label = this.label;
        	return newTime;
        }
	}

	public class ColorModel {
		public String exceedTimeAllowed;
		public String winthinTimeAllowed;
		public String overTimeAllowed;
		public String noAssignedHours;
		public String fscTitle;
		public String rsaTitle;
		public String repTitle;
		public String pendingJob;
		public String inProgressJob;
		public String readyJob;
		public String completedJob;

		public ColorModel () {
			sked_Dashboard_Color__c colorSetting = sked_Dashboard_Color__c.getOrgDefaults();
			if (colorSetting == null) {
				throw new skedException(skedConstants.NO_COLOR_SETTING);
			}
			this.exceedTimeAllowed = colorSetting.Exceed_Time_Allowed__c;
			this.winthinTimeAllowed = colorSetting.Within_Time_Allowed__c;
			this.overTimeAllowed = colorSetting.Over_Time_Allowed__c;
			this.noAssignedHours = colorSetting.No_Assigned_Hour__c;
			this.fscTitle = colorSetting.FSC_Title_Color__c;
			this.rsaTitle = colorSetting.RSA_Title_Color__c;
			this.repTitle = colorSetting.Manager_Title_Color__c;
			this.pendingJob = colorSetting.Pending_Allocation_Job__c;
			this.inProgressJob = colorSetting.In_Progress_Job__c;
			this.readyJob = colorSetting.Ready_Job__c;
			this.completedJob = colorSetting.Completed_Job__c;
		}
	}

	public static List<DateSlot> initDateSlots(List<DateSlot> lstDateSlots, DateTime startDate, DateTime endDate, String timezone, ColorModel color) {
		if (lstDateSlots == null) {
			lstDateSlots = new List<DateSlot>();
		}
		DateTime tempDate = startDate;
		//get start time and end time of a date slot from custom setting
		sked_Dashboard_Settings__c setting = sked_Dashboard_Settings__c.getOrgDefaults();
		if (setting == null) {
			throw new skedException(skedConstants.NO_DASHBOAD_SETTING);
		}
		Integer startTime = skedUtils.ConvertTimeNumberToMinutes((Integer)setting.Start_Time__c);
		Integer endTime = skedUtils.ConvertTimeNumberToMinutes((Integer)setting.End_Time__c);

		while(tempDate < endDate) {
			DateSlot slot = new DateSlot();
			slot.assignedHours = new Item(0, color.noAssignedHours);
			slot.nAvaiResource = new Item(0, color.noAssignedHours);
			slot.mapAvaiResources = new Map<String, EmployeeModel>();
			slot.lstAvaiResources = new List<EmployeeModel>();
			slot.lstJobs = new List<JobModel>();
			slot.dateValue = Date.newInstance(tempDate.yearGMT(), tempDate.monthGMT(), tempDate.dayGMT());
			slot.weekDate = tempDate.format('EEE', timezone).toUpperCase();
			slot.dateString = String.valueOf(slot.dateValue);
			slot.title = slot.weekDate.subString(0,1);
			slot.slotStart = skedUtils.addMinutes(tempDate, startTime, timezone);
			slot.slotFinish = skedUtils.addMinutes(tempDate, endTime, timezone);
			slot.startTime = new TimeModel(slot.slotStart, timezone);
			slot.endTime = new TimeModel(slot.slotFinish, timezone);
			slot.isAvailable = false;
			lstDateSlots.add(slot);
			System.debug('2aa tempDate = ' + tempDate);
			System.debug('2aa slot = ' + slot);
			tempDate = skedUtils.addDays(tempDate, 1, timezone);
		}

		return lstDateSlots;
	}

	//customized class
    global class Availability {
        public String weekDate;
        public String dayString;
        public Integer startTime;
        public Integer endTime;
        public DateTime start;
        public DateTime finish;
        public Date startDate;
        public Date endDate;
        public String avaiType;
        public Boolean isAvailable;
        public String jobId;
        public String jobStatus;
    }

    public class JobTemplate {
		public String name;
		public String underlying;
		public String orgId;
		public String id;
	}

	public class JobTask {
		public String rel;
		public String field;
		public String value;
		public String templateId;
		public String id;
	}

	public class apiResponse {
		public String errorCode 	{get; set;}
		public String errorMessage 	{get; set;}
		public boolean success 		{get; set;}
	}

	public class Value {
		public String Name;
		public String Description;
		public Integer Seq;
	}

	public class LocationModel {
		//public DateTime time;
		public Decimal lat;
		public Decimal lng;
		public Decimal accuracy;
	} 

	public class JsonResult {
		public List<JobJson> result;
	}

	public class JobJson {
		public String jobId;
		public String resourceId;
		public String arrived;
		public String departed;
		public Integer travelTime;
		public Integer serviceTime;
		public Double travelDistance;
		public Double travelLength;
		public Double totalDistance;
		public Double totalLength;
		public List<Events> events;
	}

	public class Events {
		public String event;
		public String eventTime;
		public String trackTime;
		public EventLocation eventLocation;
		public String nextEvent;
		public Integer duration;
		public Double length;
		public Double distance;
	}

	public class EventLocation {
		public Double lat;
		public Double lng;
	}
}