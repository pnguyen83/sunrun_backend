@isTest
global class skedMockTest implements HttpCalloutMock
{
	global string calloutMethod {get;set;}

	global string resourceId {get;set;}

	global HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
		if (calloutMethod == 'dispatch' || calloutMethod == 'cancel') {
			res.setHeader('Content-Type', 'application/json');
			string jsonResult = '{"results":{"' + resourceId + '":{"dts":"2016-04-22T06:37:59.585Z","protocol":"n/a","success":true}}}';
			res.setBody(jsonResult);
			res.setStatusCode(200);
		}
		else if (calloutMethod == 'dispatch_error' || calloutMethod == 'cancel_error') {
			res.setHeader('Content-Type', 'application/json');
			string jsonResult = '{"results":{"' + resourceId + '":{"dts":"2016-04-22T06:37:59.585Z","protocol":"n/a","success":false,"errorMessage":"The resource is not notifiable","errorCode":"NotNotifiable"}}}';
			res.setBody(jsonResult);
			res.setStatusCode(200);
		}
        else if (calloutMethod == 'dispatch_error_salesforce_side' || calloutMethod == 'cancel_error_salesforce_side') {
			res.setHeader('Content-Type', 'application/json');
			string jsonResult = '<html>error</html>';
			res.setBody(jsonResult);
			res.setStatusCode(200);
		}
		else if (calloutMethod == 'jobtask') {
			res.setHeader('Content-Type', 'application/json');
			string jsonResult = '[{"name":"Shift","underlying":"Jobs","orgId":"c42a2965-c926-48de-956b-f674e0720717","id":"14b0e6fa-1e38-40c7-8976-722ea02a056d"}]';
			res.setBody(jsonResult);
			res.setStatusCode(200);
			this.calloutMethod = '';
		}
		else if (calloutMethod == '') {
			res.setHeader('Content-Type', 'application/json');
			string jsonResult = '[{"rel":"job","field":"JobTasks","value":"[{\\"Name\\":\\"Complete Kiosk Location details\\",\\"Description\\":\\"Attach picture of Kiosk Location\\",\\"Seq\\":0}]","templateId":"14b0e6fa-1e38-40c7-8976-722ea02a056d","id":"c081539e-f7f3-46f2-afbe-c28182cd5c4e"}]';
			res.setBody(jsonResult);
			res.setStatusCode(200);
		}
		return res;
	}
}