global class skedSkeduloApiManager extends skedSunRunModel{
	@future(callout=true)
    global static void dispatchJobs_future(set<Id> jobIds) {
        dispatchJobs(jobIds);
    }
    
    global static void dispatchJobs(set<Id> jobIds) {
        List<sked__Job__c> jobResults = [SELECT Id, Name, 
                                         (SELECT Id, sked__Resource__c, sked__Status__c, sked__Resource__r.sked__Resource_Type__c, sked_Sent_Notification__c
                                          FROM sked__Job_Allocations__r
                                          WHERE sked_Sent_Notification__c = false
                                          AND (sked__Status__c = :skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED 
                                                OR sked__Status__c = :skedConstants.JOB_ALLOCATION_STATUS_DISPATCHED
                                                OR sked__Status__c = :skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH))
                                         FROM sked__Job__c
                                         WHERE Id IN :jobIds];
        if (!jobResults.isEmpty()) {
            List<sked__Job_Allocation__c> jobAllocsToUpdate = new List<sked__Job_Allocation__c>();
            for (sked__Job__c job : jobResults) {
                sendDispatchNotification(job, jobAllocsToUpdate);
            }
            update jobResults;
            if (!jobAllocsToUpdate.isEmpty()) {
                update jobAllocsToUpdate;
            }
        }
    }
    
    private static void sendDispatchNotification(sked__Job__c job, List<sked__Job_Allocation__c> jobAllocsToUpdate) {
        apiResponse result = dispatchJob(job, jobAllocsToUpdate);
        system.debug('#result dispatch Job#' + result);
        if (result.success == FALSE) {
            if (!string.isBlank(result.errorCode)) {
                throw new skedException('Error code: ' + result.errorCode);
            }
            if (!string.isBlank(result.errorMessage)) {
                throw new skedException('Error Message: ' + result.errorMessage);
            }
        } else {
            job.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
        }
        system.debug('#job#' + job);
    }
    
    private static apiResponse dispatchJob(sked__Job__c job, List<sked__Job_Allocation__c> jobAllocsToUpdate) {
        apiResponse result = new apiResponse();
        Http http      = new Http();
        HttpRequest req    = new HttpRequest();
        HttpResponse res  = new HttpResponse();
        
        sked_API_Setting__c skeduloApiSetting = sked_API_Setting__c.getOrgDefaults();
        if (string.isBlank(skeduloApiSetting.API_Token__c)) {
            result.success = FALSE;
            result.errorMessage = 'Skedulo API Error: API Token is null.';
            return result;
        }
        
        //Set end point to Authenciate
        string EndPoint = 'https://app.skedulo.com/dispatch?';
        string jobParams = EncodingUtil.urlEncode(job.Id, 'UTF-8');
        EndPoint = EndPoint + 'job=' + jobParams;
        
        req.setEndpoint( EndPoint );
        req.setMethod('POST');
        req.setTimeout(20000);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('apiToken', skeduloApiSetting.API_Token__c);
        
        string jsonResponse;
        try {
            res = http.send(req);
            jsonResponse = res.getBody();
            
            Map<string, object> deserializedResponse = (Map<string, object>)Json.deserializeUntyped(jsonResponse);
            Map<string, object> resultsObject = (Map<string, object>)deserializedResponse.get('results');
            System.debug('deserializedResponse#' + deserializedResponse);
            for (sked__Job_Allocation__c jobAlloc : job.sked__Job_Allocations__r) {
                string resourceResponseString = Json.serialize(resultsObject.get(jobAlloc.sked__Resource__c));
                apiResponse allocResponse = (apiResponse)Json.deserialize(resourceResponseString, apiResponse.class);
                System.debug('ApiResponse#' + allocResponse);
                if (allocResponse.success == FALSE) {
                    if (!string.isBlank(allocResponse.errorCode)) {
                        throw new skedException('Error Code: ' + allocResponse.errorCode);
                    }
                    if (!string.isBlank(allocResponse.errorMessage)) {
                        throw new skedException('Error Message: ' + allocResponse.errorMessage);
                    }
                    jobAllocsToUpdate.add(jobAlloc);
                } else {
                    if (jobAlloc.sked_Sent_Notification__c == false) {
                        jobAlloc.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;
                        jobAlloc.sked_Sent_Notification__c = true;
                        jobAllocsToUpdate.add(jobAlloc);
                    }
                }
            }
        }
        /*catch (JSONException jsonEx) {
            result.success = FALSE;
            result.errorMessage = 'An error happens in Skedulo Server. Please contact administrator.';
            result.errorMessage += (' Json Response: ' + jsonResponse);
        } */
        catch(Exception ex) {
            result.success = FALSE;
            //result.errorMessage = ('Error: ' + ex.getMessage());
            //result.errorMessage += (' Stack Trace: ' + ex.getStackTraceString());
            //System.debug('result.errorMessage#' + result.errorMessage);
        }
        system.debug(result);
        return result;
    }

    @future(Callout=true)
    global static void updateJobTask(String jobId) {
    	getJobTask(jobId); 
    }

    public static void getJobTask(String jobId) {
    	List<JobTask> lstJobTasks = new List<JobTask>();
    	List<JobTemplate> lstTemplates = new List<JobTemplate>();
    	List<sked__Job_Task__c> lstResult = new List<sked__Job_Task__c>();
        List<sked__Job__c> lstJobs = new List<sked__Job__c>();
    	string jsonResponse;
    	String templateId = '';
    	String endpoint = '';
    	//SavePoint sp = Database.setSavePoint();
        
        try {
        	endpoint = 'https://app.skedulo.com/templates/Jobs';
        	jsonResponse = callSkeduloAPI(endpoint, 'GET');
        	System.debug('jsonResponse = ' + jsonResponse);
            lstTemplates = (List<JobTemplate>)Json.deserialize(jsonResponse, List<JobTemplate>.class);
            System.debug('lstTemplates = ' + lstTemplates);

            for (JobTemplate jobtem : lstTemplates) {
            	if (jobtem.name.equalsIgnoreCase(skedConstants.JOB_TYPE_SHIFT)) {
            		templateId = jobtem.id;
            		break;
            	}
            }

        	if (String.isBlank(templateId)) {
        		throw new skedException(skedConstants.NO_JOB_TYPE_SHIFT);
        	}

        	jsonResponse = '';
        	endpoint = 'https://app.skedulo.com/template/' + templateId + '/values';
        	jsonResponse = callSkeduloAPI(endpoint, 'GET');
        	System.debug('jsonResponse = ' + jsonResponse);
        	lstJobTasks = (List<JobTask>)Json.deserialize(jsonResponse, List<JobTask>.class);
        	System.debug('lstJobTasks =' + lstJobTasks);
        	for (JobTask task : lstJobTasks) {
        		if (task.field.equalsIgnoreCase(skedConstants.JOB_TASK_TYPE)) {
        			List<Value> lstvalues = (List<Value>)Json.deserialize(task.value, List<Value>.class);
                    //List<Value> lstvalues = task.value;
                    if (lstvalues != null) {
                        for (Value vl : lstvalues) {
                            sked__Job_Task__c jobTask = new sked__Job_Task__c(
                                Name = vl.Name,
                                sked__Description__c = vl.Description,
                                sked__Seq__c = vl.Seq,
                                sked__Job__c = jobId
                            );
                            lstResult.add(jobTask);
                        }    
                    }
        			
				}

                if (task.field.equalsIgnoreCase(skedConstants.JOB_CAN_BE_DECLINED)) {
                    boolean isDeclined = task.value.equalsIgnoreCase('true') ? true : false;
                    sked__Job__c job = new sked__Job__c(
                        id = jobid,
                        sked__Can_Be_Declined__c = isDeclined
                    );
                    lstJobs.add(job);
                }
        	}

        	System.debug('lstResult = ' + lstResult);
        	if (!lstResult.isEmpty()) {
        		insert lstResult;
        	}

            if (!lstJobs.isEmpty()) {
                update lstJobs;
            }
            
    	} catch(Exception ex) {
    		//Database.rollback(sp);
           	System.debug(' Stack Trace: ' + ex.getStackTraceString());
            throw new skedException('Error: ' + ex.getMessage());
        }
    }

    public static String callSkeduloAPI(String endPoint, String method) {
    	string jsonResponse;
    	Http http      = new Http();
        HttpRequest req    = new HttpRequest();
        HttpResponse res  = new HttpResponse();  

        sked_API_Setting__c skeduloApiSetting = sked_API_Setting__c.getOrgDefaults();

        if (string.isBlank(skeduloApiSetting.API_Token__c)) {
            throw new skedException('Skedulo API Error: API Token is null.');
        }

        //Set end point to Authenciate
        //string EndPoint = 'https://app.skedulo.com/templates/Jobs';
        req.setEndpoint( EndPoint );
        req.setMethod(method);
        req.setTimeout(20000);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('apiToken', skeduloApiSetting.API_Token__c);

        try {
        	res = http.send(req);
            jsonResponse = res.getBody();
        } catch (JSONException jsonEx) {
            throw new skedException('An error happens in Skedulo Server. Please contact administrator. Json Response: ' + jsonResponse);
        }
        catch(Exception ex) {
           	System.debug(' Stack Trace: ' + ex.getStackTraceString());
            throw new skedException('Error: ' + ex.getMessage());
        }

        return jsonResponse;
    }

    //Update check in/check out distance for job
    @future(Callout=true)
    global static void updateJobDistance(Set<Id> setAllocationIds) {
        updateJobCheckInCheckOutDistance(setAllocationIds); 
    } 

    public static void updateJobCheckInCheckOutDistance(Set<Id> setAllocationIds) {
        Map<String, sked__Job_Allocation__c> mapAllocation = new Map<String, sked__Job_Allocation__c>();
        String method = 'GET';
        String endpoint = 'callout:Skedulo_API/travel/summary/job/jobs?job_ids=';
        Map<String, Location> mapJobLocation = new Map<String, Location>();
        Map<Id, sked__Job__c> mapJobs = new Map<id, sked__Job__c>();
        String lstJobIds = '';  

        for (sked__Job_Allocation__c allocation : [SELECT Id, sked__Status__c, sked__Job__c, sked__Job__r.sked_Check_In_Distance__c, 
                                                    sked__Job__r.sked_Check_Out_Distance__c ,sked__Resource__c
                                                    FROM sked__Job_Allocation__c
                                                    WHERE id IN : setAllocationIds
                                                    ]) {
            mapAllocation.put(allocation.sked__Job__c, allocation);
        }

        for (sked__Job__c job : [SELECT Id, sked__GeoLocation__c FROM sked__Job__c WHERE Id IN : mapAllocation.keySet()]) {
            if (job.sked__GeoLocation__c != null) {
                mapJobLocation.put(job.id, job.sked__GeoLocation__c);
            }
        }

        for (String jobId : mapAllocation.keySet()) {
            lstJobIds += jobId +',';
        }

        lstJobIds = lstJobIds.removeEnd(',');
        endpoint += lstJobIds;
        endpoint += '&events=true';
        System.debug('Endpoint = ' + endpoint);
        try {
            String jsonResponse = skedSkeduloApiManager.callSkeduloAPI(endpoint, method);
            System.debug('jsonResponse = ' + jsonResponse);
            JsonResult response = (JsonResult)Json.deserialize(jsonResponse, JsonResult.class);
            System.debug('response = ' + response);
            if (jsonResponse != null) {
                if (response.result != null) {
                    for (JobJson jobResult : response.result) {
                        String jobId = jobResult.jobId;
                        String resId = jobResult.resourceId;
                        if (mapAllocation.containsKey(jobId)) {
                            sked__Job_Allocation__c ja  = mapAllocation.get(jobId);

                            if (mapJobLocation.containsKey(jobId)) {
                                Location jobLoc = mapJobLocation.get(jobId);

                                if (jobResult.events != null) {
                                    for (Events event : jobResult.events) {
                                        System.debug('event.event = ' + event.event);
                                        System.debug('ja.sked__Status__c = ' + ja.sked__Status__c);
                                        
                                        if ((event.event == skedConstants.MOBILE_EN_ROUTE || event.event == skedConstants.MOBILE_CHECKED_IN) &&
                                                ja.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_CHECK_IN &&
                                                ja.sked__Job__r.sked_Check_In_Distance__c == null) {
                                            Location currentLoc = Location.newInstance(event.eventLocation.lat, event.eventLocation.lng);
                                            Decimal distance = currentLoc.getDistance(jobLoc, 'km');
                                            sked__Job__c updateJob = new sked__Job__c(id = jobId, sked_Check_In_Distance__c = distance);
                                            mapJobs.put(jobId, updateJob);
                                        }

                                        if ((event.event == skedConstants.MOBILE_IN_PROGRESS || event.event == skedConstants.MOBILE_COMPLETE) &&
                                            ja.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_COMPLETE &&
                                            ja.sked__Job__r.sked_Check_Out_Distance__c == null) {
                                            Location currentLoc = Location.newInstance(event.eventLocation.lat, event.eventLocation.lng);
                                            Decimal distance = currentLoc.getDistance(jobLoc, 'km');
                                            if (mapJobs.containsKey(jobId)) {
                                                sked__Job__c updateJob = mapJobs.get(jobId);
                                                updateJob.sked_Check_Out_Distance__c = distance;
                                                mapJobs.put(jobId, updateJob);
                                            } 
                                            else {
                                                sked__Job__c updateJob = new sked__Job__c(id = jobId, sked_Check_Out_Distance__c = distance);
                                                mapJobs.put(jobId, updateJob);    
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!mapJobs.isEmpty()) {
                update mapJobs.values();
            }
                
        } catch (Exception ex) {
            System.debug('Error: ' + ex.getMessage());
            System.debug('Stack: ' + ex.getStackTraceString());
        }
    }
}