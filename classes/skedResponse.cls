global class skedResponse {
	
	public boolean success 		{get; set;}
	public string  message 		{get; set;}
	public string  devMessage 	{get; set;}
	public object  result  		{get; set;}
	

	/*
	* contructor
	*/
	public skedResponse(boolean success, string message,  object result) {
		this.success = success;
		this.message = message;
		this.result = result;
	}

}