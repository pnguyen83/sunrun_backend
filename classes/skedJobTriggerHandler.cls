public with sharing class skedJobTriggerHandler {
	public static void onBeforeInsert(List<sked__Job__c> lstNew) {
		Map<String, List<sked__Job__c>> mapAccountJobs = new Map<String, List<sked__Job__c>>();
		mapAccountJobs = getMapAccountJobs(lstNew);
		updateJobAddress(mapAccountJobs);
		updateJobDescription(mapAccountJobs);
	}

	public static void onBeforeUpdate(List<sked__Job__c> lstNew, List<sked__Job__c> lstOld) {
		Map<String, List<sked__Job__c>> mapAccountJobs = new Map<String, List<sked__Job__c>>();
		mapAccountJobs = getMapAccountJobs(lstNew);
		updateJobAddress(mapAccountJobs);
		updateJobDescription(mapAccountJobs);
	}

	//=======================================================================================//
	//update job address
	public static void updateJobAddress(Map<String, List<sked__Job__c>> mapAccountJobs) {
		for (Account acc : [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry
							FROM Account WHERE Id IN :mapAccountJobs.keySet()
							]) {
			String shipAddress = String.isNotBlank(acc.ShippingStreet) ? acc.ShippingStreet : '';
			shipAddress += String.isNotBlank(acc.ShippingCity) ? ', ' + acc.ShippingCity : '';
			shipAddress += String.isNotBlank(acc.ShippingState) ? ', ' + acc.ShippingState : '';
			shipAddress += String.isNotBlank(acc.ShippingPostalCode) ? ', ' + acc.ShippingPostalCode : '';
			shipAddress += String.isNotBlank(acc.ShippingCountry) ? ', ' + acc.ShippingCountry : '';

			List<sked__Job__c> lstJobs = mapAccountJobs.get(acc.id);
			if (lstJobs != null) {
				for (sked__Job__c job : lstJobs) {
					if (String.isNotBlank(shipAddress) && job.sked__Address__c != shipAddress) {
						job.sked__Address__c = shipAddress;
					}
				}	
			}
		}
	}

	//update job description
	public static void updateJobDescription(Map<String, List<sked__Job__c>> mapAccountJobs) {
		for (sked_Retail_Store_Slot__c retail : [SELECT Id, sked_Retail_Store__c, sked_Retail_Store__r.Name, sked_Kiosk_Location__c,
													sked_Date__c
													FROM sked_Retail_Store_Slot__c 
													WHERE sked_Retail_Store__c IN :mapAccountJobs.keySet()
												]) {
			String kioskLocation = String.isNotBlank(retail.sked_Kiosk_Location__c) ? retail.sked_Kiosk_Location__c : '';
			List<sked__Job__c> lstJobs = mapAccountJobs.get(retail.sked_Retail_Store__c);
			if (lstJobs != null) {
				for (sked__Job__c job : lstJobs) {
					if (job.sked__Start__c.Date() == retail.sked_Date__c) {
						job.sked__Description__c = retail.sked_Retail_Store__r.Name + ' - ' + kioskLocation;	
					}
				}
			}
			
		}
	}

	//get map Account - Job from list of new jobs
	public static Map<String, List<sked__Job__c>> getMapAccountJobs(List<sked__Job__c> lstNew) {
		Map<String, List<sked__Job__c>> mapAccountJobs = new Map<String, List<sked__Job__c>>();
		
		for (sked__Job__c job : lstNew) {
			List<sked__Job__c> lstJobs = mapAccountJobs.get(job.sked__Account__c);
			if (lstJobs == null) {
				lstJobs = new List<sked__Job__c>();
			}
			lstJobs.add(job);
			mapAccountJobs.put(job.sked__Account__c, lstJobs);
		}

		return mapAccountJobs;
	}
}