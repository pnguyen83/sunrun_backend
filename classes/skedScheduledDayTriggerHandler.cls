public class skedScheduledDayTriggerHandler {
	public static void onAfterUpdate(Map<Id, sked_Retail_Store_Slot__c> mapNew, Map<Id, sked_Retail_Store_Slot__c> mapOld) {
		updateRealtedShift(mapNew, mapOld);
	}

	//==========================================================Execution===================================================//
	//when a schedule day is cancelled, all realted shift and job allocation
	public static void updateRealtedShift(Map<Id, sked_Retail_Store_Slot__c> mapNew, Map<Id, sked_Retail_Store_Slot__c> mapOld) {
		Set<String> setRegionIds = new Set<String>();
		Set<String> setStoreIds = new Set<String>();
		List<sObject> lstObjects = new List<sObject>();
		Map<Date, List<sked_Retail_Store_Slot__c>> mapDays = new Map<Date, List<sked_Retail_Store_Slot__c>>();
		
		for (sked_Retail_Store_Slot__c day : mapNew.values()) {
			if (mapOld.containsKey(day.id)) {
				sked_Retail_Store_Slot__c oldDay = mapOld.get(day.id);
				if (day.sked_Status__c != null && day.sked_Status__c.contains('Cancelled') && 
					(oldDay.sked_Status__c == null || !oldDay.sked_Status__c.contains('Cancelled'))) {
					setRegionIds.add(day.sked_Territory__c);
					setStoreIds.add(day.sked_Retail_Store__c);
					List<sked_Retail_Store_Slot__c> lstDays = mapDays.get(day.sked_Date__c);

					if (lstDays == null) {
						lstDays = new List<sked_Retail_Store_Slot__c>();
					}
					lstDays.add(day);
					mapDays.put(day.sked_Date__c, lstDays);
				}
			}
		}

		for (sked__Job__c job : [SELECT Id, Name, sked__Job_Allocation_Count__c, sked__Job_Status__c, sked__Start__c, 
									sked__Region__c, sked__Account__c,
									(SELECT Id, sked__Status__c FROM sked__Job_Allocations__r
									WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_COMPLETE
									AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED)
								FROM sked__Job__c
								WHERE sked__Job_Status__c != :skedConstants.JOB_STATUS_CANCELLED
								AND sked__Region__c IN : setRegionIds
								AND sked__Account__c IN : setStoreIds
								]) {
			Date jobDay = job.sked__Start__c.date();
			if (mapDays.containsKey(jobDay)) {
				List<sked_Retail_Store_Slot__c> lstDays = mapDays.get(jobDay);

				if (lstDays != null) {
					for (sked_Retail_Store_Slot__c day : lstDays) {
						if (day.sked_Territory__c == job.sked__Region__c && day.sked_Retail_Store__c == job.sked__Account__c) {
							job.sked__Job_Status__c = skedConstants.JOB_STATUS_CANCELLED;
			
							if (job.sked__Job_Allocation_Count__c > 0) {
								for (sked__Job_Allocation__c allocation : job.sked__Job_Allocations__r) {
									allocation.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
									lstObjects.add(allocation);
								}
							}

							lstObjects.add(job);
						}
					}
				}
			}
			
		}

		if (!lstObjects.isEmpty()) {
			lstObjects.sort();
			update lstObjects;
		}
	}
}