@isTest
public class skedSunRunTest extends skedSunRunModel
{
	@isTest
	public static void getDashboardTest()
	{
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		test.startTest();
		skedModalController.getDashboard(String.valueof(System.today()));
		test.stopTest();
	}

	@isTest public static void getListRegionsTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		test.startTest();
		skedModalController.getListRegions();
		test.stopTest();
	}

	@isTest public static void getSettingTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.getSetting(store.id, String.valueof(Date.today()));
		test.stopTest();
	}

	@isTest public static void getSettingTest2() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.getSetting('', String.valueof(Date.today()));
		test.stopTest();
	}

	@isTest public static void getSettingTest3() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.getSetting('abc', String.valueof(Date.today()));
		test.stopTest();
	}

	@isTest public static void getSettingTest4() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		Account store = (Account)mapData.get('account 2');
		test.startTest();
		skedModalController.getSetting(store.id, String.valueof(Date.today()));
		test.stopTest();
	}

	@isTest public static void getListStoresTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		test.startTest();
		skedModalController.getListStores(String.valueOf(Date.today()), region.id);
		test.stopTest();
	}

	@isTest public static void getListActiveEmployeesTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		test.startTest();
		skedModalController.getListActiveEmployees(region.id, String.valueOf(Date.today()));
		test.stopTest();
	}

	@isTest public static void getStoreInformationTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.getStoreInformation(region.id, store.id, String.valueOf(Date.today()));
		test.stopTest();
	}

	@isTest public static void saveJobTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 900, 1400, store.id, region.id, '');
		test.stopTest();
	}

	@isTest public static void saveJobTest2() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		sked__Job__c job = (sked__Job__c)mapData.get('job');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today().addDays(1)), 900, 1400, store.id, region.id, job.id);
		test.stopTest();
	}

	@isTest public static void saveJobTest22() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today().addDays(6)), 1400, 1800, store.id, region.id, null);
		test.stopTest();
	}

	@isTest public static void saveJobTest2a() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		sked__Job__c job = (sked__Job__c)mapData.get('job a');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 900, 1400, store.id, region.id, job.id);
		test.stopTest();
	}

	@isTest public static void saveJobTest2b() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		sked__Job__c job = (sked__Job__c)mapData.get('job b');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 900, 1400, store.id, region.id, job.id);
		test.stopTest();
	}

	@isTest public static void saveJobTest3() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 900, 1000, store.id, region.id, '');
		test.stopTest();
	}

	@isTest public static void saveJobTest4() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today().addDays(-1)), 900, 1800, store.id, region.id, '');
		test.stopTest();
	}

	@isTest public static void saveJobTest5() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 600, 1400, store.id, region.id, '');
		test.stopTest();
	}

	@isTest public static void saveJobTest6() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__region__c region = (sked__region__c)mapData.get('region 1');
		Account store = (Account)mapData.get('account 1');
		test.startTest();
		skedModalController.saveJob(String.valueOf(Date.today()), 1000, 2200, store.id, region.id, '');
		test.stopTest();
	}

	@isTest public static void assignEmployeeTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__Job__c job = (sked__Job__c)mapData.get('job');
		sked__Resource__c res = (sked__Resource__c)mapData.get('rsa');

		test.startTest();
		skedModalController.assignEmployee(job.id, res.id);
		test.stopTest();
	}

	@isTest public static void deleteJobAllocationTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__Job__c job = (sked__Job__c)mapData.get('job');
		sked__Job_Allocation__c allocation = (sked__Job_Allocation__c)mapData.get('allocation 1');

		test.startTest();
		skedModalController.deleteJobAllocation(allocation.id, job.id);
		test.stopTest();
	}

	@isTest public static void deleteJobTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__Job__c job = (sked__Job__c)mapData.get('job');
		
		test.startTest();
		skedModalController.deleteJob(job.id);
		test.stopTest();
	}	

	@isTest public static void autoAllocateEmployeeTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		Account store = (Account)mapData.get('account 1');
		sked__region__c region = (sked__region__c)mapData.get('region 1');

		test.startTest();
		skedModalController.autoAllocateEmployee(region.id, store.id, String.valueOf(Date.today()));
		test.stopTest();

	}

	@isTest public static void createJobAllocationTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked__Job__c job = (sked__Job__c)mapData.get('job');
		JobModel jobMod = new JobModel(job, 'America/Los_Angeles');
		Allocation all = new Allocation(jobMod, 'America/Los_Angeles');
		List<Allocation> lstAllocation = new List<Allocation>{all};

		test.startTest();
		skedModalController.createJobAllocation(lstAllocation);
		test.stopTest();
	}

	@isTest public static void cancelledScheduledDayTest() {
		Map<String, sObject> mapData = skedTestDataFactory.createData();
		sked_Retail_Store_Slot__c day = (sked_Retail_Store_Slot__c)mapData.get('retail store slot0');

		test.startTest();
		day.sked_Status__c = skedConstants.SCHEDULED_DAY_CANCELLED_SUNRUN;
		update day;
		test.stopTest();
	}
}