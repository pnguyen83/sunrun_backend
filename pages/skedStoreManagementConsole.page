<apex:page showHeader="true" sidebar="false" controller="skedModalController" standardStylesheets="false">
  <head>
    <meta charset="UTF-8" />
    <title>SunRun</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <link rel="stylesheet"  href="{!URLFOR($Resource.skedSRAssets, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery-ui-custom/jquery-ui.min.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery-ui-custom/jquery-ui.theme.min.css')}" />
    <!-- Styles -->
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/vendor/toastr/toastr.min.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/styles/selectWeek.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/styles/asideMenu.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/styles/popover.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/styles/contextMenu.css')}" />
    <link rel="stylesheet" href="{!URLFOR($Resource.skedSRDist, 'dist/styles/main.css')}" />

    <style>
      body.slds, td,
      h1, h2, h3, h4, h5, h6 {
        font-family: "Salesforce Sans",Arial,sans-serif !important;
      }

      table#phHeader {
        display: none !important;
      }

      body.sfdcBody { 
        background: none;
      }

      .bPageFooter {
        padding-bottom: 10px;
        visibility: hidden;
      }

      .bodyDiv {
        visibility: hidden;
      }

      body button, body .x-btn, body .btn, body .btnDisabled, body .btnCancel, body .menuButton .menuButtonButton {
        font-weight: normal;
      }

      ul li, ol li {
        margin-left: 0;
      }

      .store-management-screen .left-side-bar {
        height: calc(100vh - 233px);
      }

      .store-management-screen .store-content .create-job-popover {
        margin-top: 0;
        margin-bottom: 0;
        background-color: transparent;
      }
    </style>
    <script>
      window.ASSET_PATH = '{!URLFOR($Resource.skedSRAssets, "/")}';
      window.DIST_PATH = '{!URLFOR($Resource.skedSRDist, "/")}';
    </script>
  </head>

  <body class="slds" id="root" style="margin: 0; padding: 0; overflow: hidden;">
    <!-- SLDS -->
    <div ng-controller="mainCtrl as main" class="ng-cloak">
      <aside-menu id="left-menu" class="side-menu" side="left" width="225px" is-backdrop="false" push-content="true">
        <div class="header">
          <img class="brand-image" ng-src="{{$root.getImageUrl('sunrun-logo.png')}}"/>
        </div>
        <ul class="menu">
            <li class="menu-item" aside-menu-toggle="left-menu" ui-sref="dashboard" ui-sref-active="active">
              Dashboard
            </li>
            <li class="menu-item" aside-menu-toggle="left-menu" ui-sref="stores" ui-sref-active="active">
              Allocate Shifts
            </li>
        </ul>
      </aside-menu>
      <aside-menu-content>
        <div class="container fixed-header-layout">
          <div class="header slds-text-align--center">
            <div class="hamburger hamburger--arrow" aside-menu-toggle="left-menu" ng-class="{'is-active': main.menuOpened}">
              <div class="hamburger-box">
                <div class="hamburger-inner"></div>
              </div>
            </div>
            <div class="page-header">
              Sunrun Rep Management
            </div>
          </div>
          <div class="content container">
            <div class="content-body">
              <ui-view></ui-view>
            </div>
          </div>
        </div>
      </aside-menu-content>
    </div>

    <!-- Libs -->
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/lodash.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/moment.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/toastr/toastr.min.js')}"></script>
    <!-- <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery.aljs-init.min.js')}"></script> -->
    <!-- <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery.aljs-modal.min.js')}"></script> -->
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/jquery-ui-custom/jquery-ui.min.js')}"></script>

    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/angular.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/angular-ui-router.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/angular-aside-menu.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/angular-animate.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/angular-sanitize.min.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/ng-jquery-ui-date-picker.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/vendor/ng-context-menu.min.js')}"></script>

    <!-- Scripts -->
    <!-- <script src="{!URLFOR($Resource.skedSRDist, 'dist/scripts/aljsInit.js')}"></script> -->
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/scripts/appUtils.js')}"></script>
    <!-- Directives -->
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/directives/sldsModalDir.js')}"></script>
    <!-- Components -->
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/components/selectWeek.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/components/dashboardScreen.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/components/storeManagementScreen.js')}"></script>
    <script src="{!URLFOR($Resource.skedSRDist, 'dist/components/autoAllocateResultModal.js')}"></script>

    <!-- API -->
    <script>
      (function(window, angular) {
      angular.module('app.api', [])
        .factory('api', ['$q', '$http', '$timeout', function($q, $http, $timeout){
          function getJson (name) {
            var deferred = $q.defer();
            $http.get(window.DIST_PATH + 'dist/jsonData/' + name + '.json')
              .then(function (response) {
                if (response.status === 200) {
                  $timeout(function(){
                    deferred.resolve(response.data)
                  }, 0)
                }
              })
            return deferred.promise;
          }

          return {
            getDashboardData: function (selectedDate) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getDashboard}', selectedDate,                       
                function(data, event) {
                  console.log('getDashboardData', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                        
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
              // console.log('-- getDashboardData', selectedDate);
              // return getJson('dashboardData');
            },

            getStoreManagementConfig: function (storeId, selectedDate) {
              // console.log('-- getStoreManagementConfig', selectedDate);
              // return getJson('storeManagementConfig');
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getSetting}', storeId, selectedDate,                  
                function(data, event) {
                  console.log('getStoreManagementConfig', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            getListStores: function (selectedDate, regionId) {
              // console.log('-- getStoreManagementConfig', selectedDate);
              // return getJson('storeManagementConfig');
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getListStores}', selectedDate, regionId,                       
                function(data, event) {
                  console.log('getListStores', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            getListStores: function (selectedDate, regionId) {
              // console.log('-- getStoreManagementConfig', selectedDate);
              // return getJson('storeManagementConfig');
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getListStores}', selectedDate, regionId,                       
                function(data, event) {
                  console.log('getListStores', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            getAvailableResources: function (regionId, selectedDate) {
              // console.log('-- getAvailableResources', selectedDate);
              // return getJson('availableResourcesData');
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getListActiveEmployees}', regionId, selectedDate,                       
                function(data, event) {
                  console.log('getAvailableResources', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            getStoreManagementData: function (regionId, storeId, selectedDate) {
              // console.log('-- getStoreManagementData', storeId);
              // return getJson('storeManagementData');
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.getStoreInformation}', regionId, storeId, selectedDate,                       
                function(data, event) {
                  console.log('getStoreManagementData', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            saveJob: function (job) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.saveJob}', job.dateString, job.startTime, job.endTime, job.storeId, job.regionId, job.jobId,
                function(data, event) {
                  console.log('saveJob', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            allocateResource: function (jobId, resourceId) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.assignEmployee}', jobId, resourceId,                    
                function(data, event) {
                  console.log('allocateResource', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            deleteJob: function (jobId) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.deleteJob}', jobId,                       
                function(data, event) {
                  console.log('deleteJob', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            deleteJobAllocation: function (allocationId, jobId) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.deleteJobAllocation}', allocationId, jobId,                       
                function(data, event) {
                  console.log('deleteJobAllocation', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            autoAllocateEmployee: function (regionId, storeId, selectedDate) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.autoAllocateEmployee}', regionId, storeId, selectedDate,                       
                function(data, event) {
                  console.log('autoAllocateEmployee', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            },

            createJobAllocation: function (allocations) {
              var deferred = $q.defer();
              Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.skedModalController.createJobAllocation}', allocations,                       
                function(data, event) {
                  console.log('createJobAllocation', data);
                    if (event.status) {                                
                        if(!data.success) {
                          return deferred.reject(data.message);
                        }
                        deferred.resolve(data.result);
                    } else {
                        //show error message.
                        deferred.reject(event.message);                              
                    }
                },
                { buffer: true, escape: false, timeout: 30000 }
              );
              return deferred.promise;
            }
          }
        }])
      })(window, angular);
    </script>

    <!-- Main -->
    <script>
      $(document).ready(function() {
        //config toastr
        toastr.options = {
          "progressBar": true,
          // "preventDuplicates": true,
          "newestOnTop": false,
          "timeOut": "10000",
          "extendedTimeOut": "5000"
        }

        var totalSFLayoutHeight = function() {
          var globalHeaderHeight = $('.globalHeaderBar').outerHeight();
          var headerHeight = $('.bPageHeader').outerHeight();
          var footerHeight = $('.bPageFooter').outerHeight();
          return globalHeaderHeight + headerHeight + footerHeight;
        }

        var fixBodyHeight = function(){
          $('.bodyDiv').css('height', 'calc(100vh - ' + totalSFLayoutHeight() + 'px)');
          $('.bodyDiv').css('visibility', 'visible');
          $('.bPageFooter').css('visibility', 'visible');
          $('body.sfdcBody').css('background', '#1797c0');
        }
        
        $(window).on('resize', function() {
          fixBodyHeight();
        })
        fixBodyHeight();
      });

      angular.module('app', ['ngAnimate', 'app.utils', 'ui.router', 'asideModule', 'ui.dashboardScreen', 'ui.storeManagementScreen'])
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
          var dashboard = {
            name: 'dashboard',
            url: '/',
            template: '<dashboard-screen></dashboard-screen>'
          }

          var stores = {
            name: 'stores',
            url: '/stores?storeId',
            template: '<store-management-screen></store-management-screen>'
          }

          $stateProvider.state(dashboard);
          $stateProvider.state(stores);

          $urlRouterProvider.otherwise('/');
        }])
        .factory('globalConfig', function() {
          var globalConfig = {
            selectedWeekDate: '',
            shiftConfig: {
              minDuration: '{!$Setup.sked_Shift_Setting__c.Min_Duration__c}',
              maxDuration: '{!$Setup.sked_Shift_Setting__c.Max_Duration__c}'
            }
          };

          return {
            update: function(newConfig) {
              globalConfig = _.extend(globalConfig, newConfig);
            },
            get: function(){
              return globalConfig;
            }
          }
        })
        .controller('mainCtrl', ['$rootScope', '$scope', 'initUtils', function ($rootScope, $scope, initUtils) {
          var $ctrl = this;

          $ctrl.$onInit = function () {
            initUtils($rootScope);
          }

          $scope.$on("getMenuState", function (event, data) {
            $scope.$apply(function () {
              $ctrl.menuOpened = data;
            });
          });
        }]);

      angular.bootstrap(document.getElementById('root'), ['app'])
    </script>
  </body>

</apex:page>