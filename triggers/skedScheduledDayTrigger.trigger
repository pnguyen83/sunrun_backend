trigger skedScheduledDayTrigger on sked_Retail_Store_Slot__c (after update) {
	if (trigger.isAfter) {
		if (trigger.isUpdate) {
			skedScheduledDayTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
		}
	}
}