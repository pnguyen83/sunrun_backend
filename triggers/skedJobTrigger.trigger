trigger skedJobTrigger on sked__Job__c (before insert, before update) {
	if (trigger.isBefore) {
		if (trigger.isInsert) {
			skedJobTriggerHandler.onBeforeInsert(trigger.new);
		} else if (trigger.isUpdate) {
			skedJobTriggerHandler.onBeforeUpdate(trigger.new, trigger.old);
		}
	}
}