trigger skedUpdateJobAllocationHours on sked__Job_Allocation__c (before Update, after update) {
    if (trigger.isBefore) {
        if (Trigger.isUpdate) {
            // Calculate the number of hours between In Progress and Completed and record the result as the number of hours worked by the resource
            skedJobAllocationTriggerHandler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
        }    
    }

    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            skedJobAllocationTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
        }
    }

}